#!/bin/bash

# area_figura_plana - cálcula área de algumas figuras plana
# autor - dlimapa@gmail.com

clear

# Coloca em loop infinito até digitar algo diferente do esperado
while :
do
    echo
    echo "Indique qual figura deseja calcular a área:"
    echo
    echo "[Q] Quadrado"
    echo "[R] Retângulo"
    echo "[TR] Triângulo retângulo"
    echo "[TE] Triângulo equilátero"
    echo "[TD] Triângulo escaleno"
    echo "[C] Circunferência"
    echo "[T] Trapézio"
    echo "[L] Losango"
    echo "[HE] Hexágono regular"
    echo "[S] Sair"
    echo
    read FIGURA
    
    # Colocando para caixa alta
    FIGURA=$(echo $FIGURA | tr /a-z/ /A-Z/)

    # Usando a estrutura de case para chamar a função de cálculo correta
    case $FIGURA in
        "Q") 
            echo "Informe o lado do quadrado:"
            read LADO
            AREA=$(($LADO**2))
            echo "Área do quadrado de lado $LADO: $AREA"
            ;;
        "R")
            echo "Informe o lado maior do retângulo:"
            read LADO_MAIOR
            echo "Informe o lado menor do retângulo:"
            read LADO_MENOR
            AREA=$(($LADO_MAIOR * $LADO_MENOR))
            echo "Área do retângulo com lados $LADO_MAIOR e $LADO_MENOR: $AREA"
            ;;
        
        "TR") 
            echo "Informe a base:"
            read BASE
            echo "Informe a altura:"
            read ALTURA
            AREA=$((($BASE * $ALTURA)/2))
            echo "Área do triângulo retângulo com base $BASE e altura $ALTURA: $AREA"
            ;;
        "TE") 
            echo "Informe o lado do triângulo equilátero:"
            read LADO
            # Raiz quadrada de três
            RAIZ=$(echo "sqrt(3)" | bc)
            
            AREA=$((($LADO**2 * $RAIZ)/4))
            echo "Área do triângulo equilátero de lado $LADO: $AREA"
            ;;
        "TD")
            echo "Informe o primeiro lado do triângulo escaleno:"
            read A
            echo "Informe o segundo lado do triângulo escaleno:"
            read B
            echo "Informe o terceiro lado do triângulo escaleno:"
            read C
            
            if (test $A -eq $B) || (test $A -eq $C) || (test $C -eq $B) then
                echo "O triângulo escaleno DEVE POSSUIR todos os lados diferentes."
            else
                P=$((($A+$B+$C)/2))
                AREA=$(( $P * ($P-$A) * ($P - $B) * ($P - $C) ))
                AREA=$(echo "sqrt($AREA)" | bc)
                echo "Área do triângulo escaleno com lados $A, $B e $C: $AREA"
            fi
            ;;
        "C")
            echo "Informe o raio da circunferência:"
            read RAIO
            
            AREA=$(( 3,14 * $RAIO**2))
            echo "Área da cricunferência de raio $RAIO: $AREA"
            ;;
        "L")
            echo "Informe a diagonal menor do losango:"
            read DIAGONAL_MENOR
            echo "Informe a diagonal maior do losango:"
            read DIAGONAL_MAIOR
            
            AREA=$((($DIAGONAL_MENOR * $DIAGONAL_MAIOR)/2))
            echo "Área do losango com diagonais $DIAGONAL_MENOR e $DIAGONAL_MAIOR: $AREA"
            ;;
        "HE")
            echo "Informe o lado do hexágono regular:"
            read LADO
            # Raiz quadrada de três
            RAIZ=$(echo "sqrt(3)" | bc)
            
            AREA=$(( (6 * $RAIZ * $LADO**2)/4 ))
            echo "Área do hexágono regular de lado $LADO: $AREA"
            ;;
        "S") 
            echo "Saindo..." 
            break;
            ;;
        *) 
            echo "Opcao desconhecida." 
            ;;
    esac
done