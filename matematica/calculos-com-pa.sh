#!/bin/bash

echo Cálculos com PA
echo
echo Informe o primeiro termo:
read A1
echo Informe o razão:
read R
echo "Informe a posição do último termo (n):"
read N

AN=$(($A1+($N-1)*$R))

SOMA=$(($N*($A1+$AN)/2))

echo "Soma dos termos da PA: $SOMA"

echo "Termos da PA:"

A=$A1
I=1

while test $A -le $AN
do
    echo Termo $I: $A
    A=$(($A+R))
    I=$(($I+1))
done 
