#!/bin/bash

# fatorial - cálculo do fatorial do número n 
# autor - dlimapa@gmail.com

echo Informe um número inteiro positivo:
read N

# Criando lista sequencial até o número informado
for I in $(seq $N)
do
    T[$I]=$I
done

# Cálculo do fatorial, definindo fatorial(1) = 1, então começamos do segundo elemento, ou seja, 2.
I=2
FAT=1
while test $I -le $N
do
    FAT=$((T[$I] * $FAT))
    I=$(($I + 1)) 
done

echo Fatorial de $N: $FAT