#!/bin/bash
# finonacci - mostra os n primeiros termos da sequência de Fibonacci
echo Mostra os n primeiros números da sequência de Fibonacci

echo Informe o número de termos:
read N

# Definindo os primeiros termos
F[0]=0
F[1]=1
F[2]=1

# Calculando conforme a formula geral F(N) = F(N-1) + F(N-2)
I=3
while test $I -le $N
do
    F[$I]=$((F[$(($I - 1))] + F[$(($I - 2))]))
    I=$(($I+1))
done

# Mostrando a sequência
echo Termos da sequência:
I=0
while test $I -le $N
do
    echo termo $I: ${F[$I]} 
    I=$(($I+1))
done