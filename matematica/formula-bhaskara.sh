#!/bin/bash

# formula-bhaskara - cálcula as raizes para equações do segundo grau
# autor - dlimapa@gmail.com

echo "Equação do segundo grau: a*x*x + b*x + c = 0"
echo
echo "Digite o primeiro coeficiente (a):"
read A
echo "Digite o primeiro coeficiente (b):"
read B
echo "Digite o primeiro coeficiente (c):"
read C

# Cálculop do discriminante
DELTA=$(($B*$B - 4*$A*$C))

if [ $DELTA -lt 0 ] 
then
    echo "Raízes complexas (DELTA = $DELTA)."
elif [ $DELTA -eq 0 ]
then
    echo "Raízes iguais e reais (DELTA = $DELTA)."
    X=$((-$B / 2 * $A))
    echo "X1=X2= $X"
else
    echo "Raízes diferentes e reais (DELTA = $DELTA)."

    # Raiz quadrada do discriminante
    RAIZ_DELTA=$(echo "sqrt($DELTA)" | bc)

    X=$(((-$B + $RAIZ_DELTA) / 2 * $A))
    echo "X1= $X"

    X=$(((-$B - $RAIZ_DELTA) / 2 * $A))
    echo "X2= $X"
fi