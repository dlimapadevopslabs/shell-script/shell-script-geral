#!/bin/bash

# imc-calc - calcula o imc

# Pegando os dados para o cálculo
echo Digite sua altura em metros:
read ALTURA
echo
echo Digite seu peso em kg:
read PESO

# Realizando o cálculo
IMC=$(($PESO/($ALTURA*$ALTURA)))

echo Seu IMC é $IMC

# Informando sobre peso normal, abaixo ou acima


if test $IMC -lt 15
then
    echo "Extremamente abaixo do peso"
elif [ $IMC -ge 15 ] && [ $IMC -lt 16 ]
then
    echo "Gravemente abaixo do pseo"
elif [ $IMC -ge 16 ] && [ $IMC -lt 18 ]
then
    echo "Abaixo do peso ideal"
elif test $IMC -ge 18 && test $IMC -lt 25
then
    echo "Faixa de peso ideal"
elif test $IMC -ge 25 && test $IMC -lt 30
then
    echo "Sobrepeso"
elif test $IMC -ge 30 && [ $IMC -lt 35 ]
then
    echo "Obesidade grau I"
elif test $IMC -ge 35 && test $IMC -lt 40
then
    echo "Obesidade grau II (grave)"
else
    echo "Obesidade grau III (mórbida)"
fi