#!/bin/bash
# sistema.sh - script que mostra informações sobre o sistema
# Autor: dlimapa@gmail.com

# Pede a confirma do usuário antes de executar
echo Vou buscar os dados do sistema. Posso continuar? [sn]
read RESPOSTA

# Se ele digitou 'n', vamos interromper o script
test "$RESPOSTA" = "n" && exit

# O date mostra a data e a hora do sistema
echo
echo Data do sistema
# Armazenando a data e hora dentro de uma variável
HOJE=$(date)
echo $HOJE

# O df mostra as partiçoes e quanto cada uma ocupa no disco
echo
echo Volumes
df -h

#O w mostra os usuários que estão conectados nesta máquina
echo
echo Usuários logados
w
echo
