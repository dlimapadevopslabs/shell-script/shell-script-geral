#!/bin/bash

# Recebe o nome a ser testado
echo Digite o arquivo:
read ARQUIVO

# Verifica se é diretório, arquivo ou não existe
test -d $ARQUIVO && echo $ARQUIVO é um diretório && exit || test -f $ARQUIVO && echo $ARQUIVO é um arquivo || echo $ARQUIVO não encontrado

